using System.Collections ;
using System.Collections.Generic ;
using UnityEngine ;

using System.Reflection ;

public class DoReflection : MonoBehaviour {

  public MonoBehaviour target ;

  void Start () {
    if ( target == null)
      return ;

    var targetType = target.GetType() ;
    MethodInfo[] methods = targetType.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly) ;

    print(methods.Length) ;
    for(int cnt = 0; cnt < methods.Length; cnt ++)
      if(methods[cnt].GetParameters().Length == 0) {
        print("Invoke " + methods[cnt].Name) ;
        methods[cnt].Invoke(target, new object[] {}) ;
      }

  }
}
