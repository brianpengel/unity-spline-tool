using System.Collections.Generic ;
using System.Collections ;
using UnityEngine ;

public partial class Challenges : MonoBehaviour {

  void Start () {
    print("// ==== Challenge 01 ==== //") ;
    List<int> nrs = new List<int> () { 5, 6, 8, 3, 6 } ;
    print(ForSum (nrs)) ;
    print(WhileSum (nrs)) ;
    print(RecursiveSum (nrs)) ;

    print("// ==== Challenge 03 ==== //") ;
    PrintList<long>(FibonacciList()) ;

    print("// ==== Challenge 04 ==== //") ;
    print(ListLargestConfiguration(new List<int> () { 420,  42, 423 })) ;
  }

  // Sum methods - Challenge 01
  public float ForSum (List<int> nrs) {
    float res = 0 ;
    for (int cnt = 0 ; cnt < nrs.Count; cnt ++)
      res += nrs[cnt] ;
    return res ;
  }

  public float WhileSum (List<int> nrs) {
    float res = 0;
    int cur = 0 ;

    while (cur < nrs.Count) {
      res += nrs[cur] ;
      cur ++ ;
    }

    return res ;
  }

  public float RecursiveSum (List<int> nrs, int step = 0) {
    if(step < nrs.Count - 1)
      return nrs[step] + RecursiveSum(nrs, step + 1) ;
    return nrs[step] ;
  }

  // Fibonacci - Challenge 03
  public List<long> FibonacciList () {
    var res = new List<long> () { 0, 1 } ;
    for(int cnt = 0 ; cnt < 98; cnt ++)
      res.Add(res[cnt] + res[cnt + 1]) ;
    return res ;
  }

  // Challenge 04
  public string ListLargestConfiguration (List<int> l) {
    var filtered = new List<string> () ;
    foreach(int nr in l)
      if(nr >= 0) filtered.Add(nr.ToString()) ;

    filtered.Sort(delegate(string a, string b) {
      int cnt = 0, maxCount = Mathf.Min(a.Length, b.Length) ;
      while(cnt < maxCount && a[cnt] == b[cnt])
        cnt ++ ;

      if(cnt == maxCount)
        return a.Length > b.Length ? 1 : -1 ;
      return (int)a[cnt] > (int)b[cnt] ? -1 : 1 ;
    });

    return string.Join("", filtered.ToArray());
  }


  // -
  public void PrintList<T> (List<T> l) {
    string str = "[" ;
    foreach(var nr in l)
      str += nr.ToString() + ", " ;
    str += "]" ;
    print(str) ;
  }
}
