using System.Collections.Generic ;
using System.Collections ;
using System.IO ;
using UnityEngine ;

public class AssetDatabaseUtil {

  public static string[] FindAssetPathsWithExtension(string fileExtension) {
    if (string.IsNullOrEmpty(fileExtension))
    return null;

    if (fileExtension[0] == '.')
    fileExtension = fileExtension.Substring(1);

    DirectoryInfo directoryInfo = new DirectoryInfo(Application.dataPath);
    FileInfo[] fileInfos        = directoryInfo.GetFiles("*." + fileExtension, SearchOption.AllDirectories);

    List<string> assetPaths     = new List<string>();

    foreach (var file in fileInfos)
    {
      var assetPath = file.FullName.Replace(@"\", "/").Replace(Application.dataPath, "Assets");
      assetPaths.Add(assetPath);
    }

    return assetPaths.ToArray();
  }
 }
