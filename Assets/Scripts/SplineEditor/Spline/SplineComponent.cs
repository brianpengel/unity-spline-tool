using UnityEngine;

namespace CustomSplines {

  public abstract class SplineComponent : MonoBehaviour {

    [SerializeField] private Spline _target ;
    public Spline target { get { return _target ; } }
    public bool isSelected { get { return UnityEditor.Selection.activeGameObject == gameObject ; } }

    public void SetTarget(Spline t) {
      _target = t ;
    }

    public Vector3 GetPointAt(float t) {
      return _target == null ? transform.position : TranslatePoint(target.GetPointAt(t)) ;
    }

    public Vector3 GetNormalAt(float t) {
      return _target == null ? transform.up : TranslatePoint(target.GetNormalAt(t)) ;
    }

    public Vector3 GetDirectionAt(float t) {
      return _target == null ? transform.forward : target.GetDirectionAt(t) ;
    }

    protected Vector3 TranslatePoint(Vector3 p) {
      return transform.position + p ;
    }

    protected Vector3 RemoveTranslation(Vector3 p) {
      return transform.position - p ;
    }
  }
}
