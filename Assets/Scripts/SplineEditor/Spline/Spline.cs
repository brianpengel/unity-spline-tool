using System.Collections.Generic ;
using System.Collections ;
using System ;
using UnityEngine ;
using TypeReferences ;

namespace CustomSplines {

  [System.Serializable]
  public class Spline : ScriptableObject, ISerializationCallbackReceiver {

     // Prebuild arrays for fast lookup
    [SerializeField] private Vector3[] controlPoints ;
    [SerializeField] private float[] cumulativeLength ;
    [SerializeField] private int[] cumulativeCount ; // Cumulative amount of handles in curves

    [SerializeField] private SerializableCurve[] serializableCurves ;
    [SerializeField] private List<Curve> curves ;

    public float Length   { get { return cumulativeLength[cumulativeLength.Length - 1] ; } }
    public int CurveCount { get { return curves.Count ; } }

    private Spline () {
      Reset() ;
    }

    public void Reset(){
    	curves = new List<Curve>() ;
      UpdateControlPoints () ;
      RecalculateLength() ;
    }

    public void Add (Curve c) {
      if(CurveCount > 1) {
        var start = curves[CurveCount - 1].LastHandle ;
        c.SetHandle(c.Count - 1, c.LastHandle - c.FirstHandle + start) ;
        c.SetHandle(0, start) ;
      }

      curves.Add(c) ;

      UpdateControlPoints () ;
      RecalculateLength() ;
      RecalculateCount() ;
    }

    public void SetControlPoint (int i, Vector3 point) {
      controlPoints[i] = point ;

      var cur = 0 ;
      while (cur + 1 < cumulativeCount.Length && i > cumulativeCount[cur + 1])
        cur ++ ;

      var addjustedIndex = i - cumulativeCount[cur] ;
      curves[cur].SetHandle(addjustedIndex, point) ;
      if(addjustedIndex == curves[cur].Count - 1 && cur < cumulativeCount.Length - 1)
        curves[cur + 1].SetHandle(0, point) ;

      UpdateControlPoints () ;
      RecalculateLength() ;
    }

    public Vector3[] GetControlPoints() {
      return (Vector3[])controlPoints.Clone();
    }

    public Vector3 GetPointAt (float t) {
      if(Length == 0) return Vector3.zero ;
      int i = GetCurveIndex(ref t) ;
      return curves[i].GetInterpolatedPoint(t) ;
    }

    public Vector3 GetNormalAt (float t) {
      if(Length == 0) return Vector3.up ;
      int i = GetCurveIndex(ref t) ;
      return curves[i].GetNormal(t, Vector3.up) ;
    }

    public Vector3 GetDirectionAt (float t) {
      if(Length == 0) return Vector3.forward ;
      int i = GetCurveIndex(ref t) ;
      return curves[i].GetTangent(t) ;
    }


    // ///////// //
    // Helpers
    // ///////// //
    private int GetCurveIndex(ref float t) {
      t = Mathf.Clamp01(t) ;
      if(curves.Count <= 1 || t == 0) return 0 ;
      if(t == 1) return curves.Count - 1 ;

      int target = cumulativeLength.Length ;
      float maxLength = cumulativeLength[target - 1],
            targetLength = maxLength * t ;

      for(int cnt = 1; cnt < target; cnt ++) {
        float current = cumulativeLength[cnt] ;
        if(current > targetLength) {
          float prev = cumulativeLength[cnt - 1] ;
          t = 1f / (current - prev) * (targetLength - prev) ;
          return cnt - 1 ;
        }
      }

      return curves.Count - 1 ;
    }

    private void RecalculateLength () {
      float[] length = new float[curves.Count + 1] ;
      length[0] = 0 ;

      for (int i = 1; i <= curves.Count; i ++) {
        int j = i - 1 ;
        length[i] = length[j] + curves[j].Length ;
      }

      cumulativeLength = length ;
    }

    private void RecalculateCount () {
      int[] count = new int[curves.Count] ;
      count[0] = 0 ;

      for (int i = 1; i < curves.Count; i ++) {
        int j = i - 1 ;
        count[i] = count[j] + curves[j].Count - 1 ;
      }

      cumulativeCount = count ;
    }

    private void UpdateControlPoints () {
      var result = new List<Vector3> () ;
      if(curves.Count == 0)
        return ;

      foreach(Curve c in curves)
        for(int cnt = 0; cnt < c.Count - 1; cnt ++)
          result.Add(c.GetHandle(cnt)) ;
      result.Add(curves[curves.Count - 1].LastHandle) ;

      controlPoints = result.ToArray() ;
    }


    // ////////////// //
    //  Serialization
    // ////////////// //
    public void OnBeforeSerialize() {
      if(curves == null) {
        serializableCurves = new SerializableCurve[0] ;
        return ;
      }

      serializableCurves = new SerializableCurve[curves.Count] ;
      for(int cnt = 0; cnt < curves.Count; cnt ++)
        if (curves[cnt] != null)
          serializableCurves[cnt] = SerializableCurve.Serialize(curves[cnt]) ;
    }

    public void OnAfterDeserialize() {
      curves = new List<Curve>() ;
      if(serializableCurves == null || serializableCurves.Length == 0) return ;
      // Debug.Log("OnAfterDeserialize") ;

      for(int cnt = 0; cnt < serializableCurves.Length; cnt ++)
        curves.Add(serializableCurves[cnt].Deserialize()) ;
    }
  }
}
