using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomSplines {

  [DisallowMultipleComponent]
  public class SplineVisualiser : SplineComponent {

    [SerializeField] private Color _renderColour = Color.white ;
    public Color renderColour { get{ return _renderColour; } set{ _renderColour = value;} }

    [SerializeField] private int  _renderResolution = 20 ;
    public int renderResolution { get{ return _renderResolution; } set{ _renderResolution = value;} }

    [SerializeField] private bool  _isVisible = true ;
    public bool isVisible { get{ return _isVisible; } set{ _isVisible = value;} }

  }
}
