using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomSplines {

  [DisallowMultipleComponent]
  public class SplineWalker : MonoBehaviour {

    public SplineComponent targetContainer ;
    public float offset = 0 ;
    public float speed ;
    public bool pong = true ;

    public void Update () {
      if(targetContainer == null)
        return ;

      var t = (Time.time + offset) * (speed / targetContainer.target.Length) ;
      var position = pong ? Mathf.PingPong(t, 1) : t % 1f;
      transform.position = targetContainer.GetPointAt(position) ;
      transform.LookAt(transform.position + targetContainer.GetDirectionAt(position)) ;
    }

  }
}
