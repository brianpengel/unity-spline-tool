using UnityEditorInternal;
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace CustomSplines {
  [InitializeOnLoad]
  [CustomEditor(typeof(Spline), true)]
  // [CanEditMultipleObjects]
  public class SplineEditor : Editor {

    private static float colourPickerOffset = 2.5f ;
    private readonly static string DEFAULT_ASSET_PATH = "Assets/New Spline.asset" ;

    static SplineEditor () {
      EditorApplication.hierarchyWindowItemOnGUI += HierarchyItemCallback;
      SceneView.onSceneGUIDelegate += SceneCallback ;
    }

    static void SceneCallback(SceneView s) {
      var e = Event.current.type ;

      if(e == EventType.DragUpdated) {
        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
      }

      else if(e == EventType.DragPerform) {
        DragAndDrop.AcceptDrag();
        var selectedObjects = new List<GameObject>();

        foreach (var objectRef in DragAndDrop.objectReferences)
            if (objectRef is Spline)
              selectedObjects.Add(CreateSplineObject((Spline) objectRef));

        if (selectedObjects.Count == 0) return;
        Selection.objects = selectedObjects.ToArray();
      }
    }

    static void HierarchyItemCallback(int instanceID, Rect r) {
      var go = (GameObject)EditorUtility.InstanceIDToObject(instanceID) ;
      if(go == null) return ;
      var visualiser = go.GetComponent<SplineVisualiser>();
      if(visualiser == null) return ;

      Rect rect = new Rect (r);
      rect.y += colourPickerOffset * .85f ;
      rect.x = rect.width;
      rect.height -= colourPickerOffset * 2;
      rect.width = rect.height ;

      visualiser.renderColour = EditorGUI.ColorField(
        rect, new GUIContent(""), visualiser.renderColour,
        false, false, false, new ColorPickerHDRConfig(0f, 1f, 0f, 1f)
      );

      rect.x += colourPickerOffset + rect.width ;
      rect.y -= colourPickerOffset ;

      // GUI.enabled = visualiser.gameObject.active ;
      bool prevEnables = visualiser.isVisible ;
      visualiser.isVisible = EditorGUI.Toggle(rect, visualiser.isVisible);
      if(prevEnables != visualiser.isVisible) SceneView.RepaintAll() ;
      // GUI.enabled = true ;
    }

    [MenuItem( "GameObject/3D Object/Spline")]
    static void OnHierarchyCreateSpline () {
      var s = ScriptableObject.CreateInstance<Spline>() ;
      AssetDatabase.CreateAsset(s, AssetDatabase.GenerateUniqueAssetPath(DEFAULT_ASSET_PATH));
      CreateSplineObject(s) ;
    }

    [MenuItem( "Assets/Create/Spline")]
    static void OnCreateSpline () {
      var s = ScriptableObject.CreateInstance<Spline>() ;
      AssetDatabase.CreateAsset(s, AssetDatabase.GenerateUniqueAssetPath(DEFAULT_ASSET_PATH));
    }

    [MenuItem( "Assets/BuddySytem/Spline with random name from xml")]
    static void OnCreateBuddySytemSpline () {
      var arr = AssetDatabaseUtil.FindAssetPathsWithExtension("xml");
      var s = ScriptableObject.CreateInstance<Spline>() ;

      if(arr.Length > 0) {
        string name = DEFAULT_ASSET_PATH ;
        var path = arr[Random.Range(0, arr.Length)] ;
        var res = AssetDatabase.LoadAssetAtPath<TextAsset>(path) ;
        var reader = XmlReader.Create(new StringReader(res.text));
        var doc = new XmlDocument() ;
        doc.Load(reader) ;

        var nodes = doc.GetElementsByTagName("name");
        if(nodes.Count == 0) doc.GetElementsByTagName("Name");
        if(nodes.Count != 0) {
          var rand = nodes[Random.Range(0, nodes.Count)] ;
          name = rand.InnerText ;
        }
        else Debug.LogWarning("No name attribute in xml: " + path) ;
        AssetDatabase.CreateAsset(s, AssetDatabase.GenerateUniqueAssetPath($"Assets/{name}.asset"));
      }
      else Debug.LogWarning("No Xml in project") ;
    }

    private static GameObject CreateSplineObject (Spline target) {
      var gameObject = new GameObject(typeof(Spline).Name);

      var builder = gameObject.AddComponent<SplineBuilder>();
      builder.SetTarget(target);

      return gameObject ;
    }

    public override void OnInspectorGUI() {
      DrawDefaultInspector() ;
    }

  }
}
