using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor ;

namespace CustomSplines {

  [CanEditMultipleObjects]
  [CustomEditor(typeof(SplineComponent), true)]
  internal class ComponentInspector : Editor {

    private SerializedProperty spline;

    public override void OnInspectorGUI() {
      SerializedProperty spline = serializedObject.FindProperty("_target");
      if(spline.objectReferenceValue != null) return ;

      EditorGUILayout.Space() ;
      EditorGUILayout.BeginVertical(EditorStyles.helpBox) ;
        EditorGUILayout.LabelField(new GUIContent("Spline Asset Missing")) ;
        EditorGUILayout.PropertyField(spline, new GUIContent("Assign:"));
        serializedObject.ApplyModifiedProperties();
      EditorGUILayout.EndVertical() ;
      EditorGUILayout.Space() ;
    }

  }
}
