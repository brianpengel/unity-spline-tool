using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor ;

namespace CustomSplines {

  [CustomEditor(typeof(SplineVisualiser))]
  internal class VisualiserInspector : Editor {

    static readonly Color SELECTION_COLOUR = Color.white ;
    static readonly int RENDER_WIDTH = 3 ;

    // private Editor inheritedInspector ;
    private SerializedProperty resolution;
    private SerializedProperty visible;
    private SerializedProperty colour;

    public void OnEnable () {
      // inheritedInspector = Editor.CreateEditor(target, typeof(ComponentInspector));
      resolution = serializedObject.FindProperty("_renderResolution");
      colour = serializedObject.FindProperty("_renderColour");
      visible = serializedObject.FindProperty("_isVisible");
    }

    public override void OnInspectorGUI() {
      // inheritedInspector.OnInspectorGUI () ;
      EditorGUI.BeginChangeCheck();
      EditorGUILayout.PropertyField(visible, new GUIContent("Should render to scene"));
      if (EditorGUI.EndChangeCheck()) SceneView.RepaintAll() ;

      EditorGUILayout.PropertyField(colour, new GUIContent("Render Colour"));
      EditorGUILayout.PropertyField(resolution, new GUIContent("Render Resolution"));
      serializedObject.ApplyModifiedProperties();
    }


    [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Pickable)]
    static void DrawVisualGizmos (SplineVisualiser visualiser, GizmoType type) {
      if(visualiser.target == null || !visualiser.isVisible || visualiser.renderResolution <= 0 || visualiser.target.Length == 0)
        return ;

      Spline spline = visualiser.target ;
      int resolution = visualiser.renderResolution ;

      Handles.color = visualiser.isSelected ? Color.Lerp(SELECTION_COLOUR, visualiser.renderColour, .3f) : visualiser.renderColour;
      Gizmos.color = Handles.color ;

      int steps =  resolution * spline.CurveCount ;
      float step = 1f/ steps ;

      Vector3[] splinePoints = new Vector3[steps + 1] ;
      splinePoints[0] = visualiser.GetPointAt(0) ;

      for(int cnt = 1; cnt < splinePoints.Length; cnt ++) {
        splinePoints[cnt] = visualiser.GetPointAt(step * cnt) ;
        Gizmos.DrawLine(splinePoints[cnt], splinePoints[cnt - 1]) ;
      }

      Handles.DrawAAPolyLine(RENDER_WIDTH, splinePoints) ;
    }

  }
}
