using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor ;

namespace CustomSplines {

  [CustomEditor(typeof(SplineBuilder))]
  internal class BuilderInspector : Editor {

    private enum BuilderTool {
      None, Draw, Move, Settings
    }

    public static readonly float HANDLE_LINE_SIZE = 1f ;
    public static readonly float HANDLE_SNAP = .01f ;
    public static readonly float HANDLE_SIZE = .05f ;

    private SplineBuilder builder ;
    private BuilderTool currentTool ;
    private Editor visualiserInspector ;
    private Editor baseInspector ;

    public void OnEnable () {
      builder = (SplineBuilder) target ;
      visualiserInspector = Editor.CreateEditor(target, typeof(VisualiserInspector));
      baseInspector = Editor.CreateEditor(target, typeof(ComponentInspector));
    }

    public void OnDisable() {
      selectedMoveHandle = -1 ;
    }

    private Vector3 TranslatePoint(Vector3 p) {
      return builder.transform.position + p ;
    }

    private Vector3 RemoveTranslation(Vector3 p) {
      return p - builder.transform.position ;
    }

    // //////////////// //
    //  UI
    // //////////////// //
    public override void OnInspectorGUI() {
      if(Tools.current != Tool.None || builder.target == null)
        currentTool = BuilderTool.None ;

      EditorGUILayout.BeginVertical ();
        baseInspector.OnInspectorGUI () ;

        DrawToolbar() ;

        switch(currentTool) {
          case BuilderTool.Draw:
            DrawToolUI() ; break ;

          case BuilderTool.Move:
            MoveToolUI() ; break ;

          case BuilderTool.Settings:
            SettingsUI() ; break ;

          default:
            NoToolUI() ; break ;
        }

        EditorGUILayout.Space() ;
      EditorGUILayout.EndVertical ();
    }

    private void DrawToolbar () {
      EditorGUILayout.Space() ;
      EditorGUILayout.BeginHorizontal ();
        GUILayout.FlexibleSpace() ;
        DrawEditToggle("ClothInspector.PaintTool", "Draw points",       BuilderTool.Draw, EditorStyles.miniButtonLeft) ;
        DrawEditToggle("MoveTool", "MoveTool On", "Move points",        BuilderTool.Move, EditorStyles.miniButtonMid) ;
        DrawEditToggle("ClothInspector.SettingsTool", "Spline Settings", BuilderTool.Settings, EditorStyles.miniButtonRight) ;
        GUILayout.FlexibleSpace() ;
      EditorGUILayout.EndHorizontal ();
      EditorGUILayout.Space() ;
    }

    private void DrawEditToggle (string iconID, string tooltip, BuilderTool targetTool, GUIStyle style) {
      DrawEditToggle (iconID, iconID, tooltip, targetTool, style) ;
    }

    private void DrawEditToggle (string iconID, string pressedIcon, string tooltip, BuilderTool targetTool, GUIStyle style) {
      bool original = currentTool == targetTool;
      var icon = EditorGUIUtility.IconContent(original ? pressedIcon : iconID) ;
      var content = new GUIContent(icon) ;
      content.tooltip = tooltip ;
      bool toggle = GUILayout.Toggle(original, content, style) ;

      if(toggle != original) {
        currentTool = toggle ? targetTool : BuilderTool.None ;
        if(toggle) Tools.current = Tool.None ;
        SceneView.RepaintAll() ;
      }
    }

    // //////////////// //
    //  Handles
    // //////////////// //
    private void OnSceneGUI () {
      if(builder.target == null) return ;

      switch(currentTool) {
        case BuilderTool.Draw:
          DoDrawTool() ;
          return ;

        case BuilderTool.Move:
          DoMoveTool() ;
          return ;
      }

    }

      // //////////////// //
      //  No Tool
      // //////////////// //
      private void NoToolUI () {
        EditorGUILayout.Space() ;
        EditorGUILayout.BeginVertical(EditorStyles.helpBox) ;
          EditorGUILayout.LabelField(new GUIContent("No tool selected")) ;
          EditorGUILayout.LabelField(new GUIContent("Please select a tool")) ;
        EditorGUILayout.EndVertical() ;
        EditorGUILayout.Space() ;
      }

      // //////////////// //
      //  Settings
      // //////////////// //
      private void SettingsUI () {
        visualiserInspector.OnInspectorGUI() ;

      }

      // //////////////// //
      //  Draw Tool
      // //////////////// //
      private enum AxisLimiter { X, Y, Z } ;
      private AxisLimiter curDrawToolLimit = AxisLimiter.Y;
      private float drawSnap = 0;
      private float planeOffset = 0f ;

      private void DrawToolUI () {
        var prev = curDrawToolLimit ;
        curDrawToolLimit = (AxisLimiter)EditorGUILayout.EnumPopup(new GUIContent("Limit drawing to plane: "), curDrawToolLimit);
        if(prev != curDrawToolLimit) planeOffset = 0;
        planeOffset = EditorGUILayout.FloatField(new GUIContent("Offset from plane:"), planeOffset);
        drawSnap = EditorGUILayout.Slider(new GUIContent("Snap points to:"), drawSnap, 0, 1f);
      }

      private void DoDrawTool () {
        var point = builder.GetPointAt(1) ;
        var cam = SceneView.lastActiveSceneView.camera ;
        var mousePos = new Vector2(Event.current.mousePosition.x, Screen.height - Event.current.mousePosition.y - 36);

        Ray ray = cam.ScreenPointToRay(mousePos);
        var normal = GetLimitedPlaneNormal(curDrawToolLimit) ;
        var planePosition = point + normal * planeOffset ;

        Plane hPlane = new Plane(normal, planePosition);
        float distance = 0;

        if (!hPlane.Raycast(ray, out distance)) return ;
        var drawPosition = ray.GetPoint(distance) ;

        int controlID = GUIUtility.GetControlID(FocusType.Passive);
        switch (Event.current.GetTypeForControl(controlID)) {
              case EventType.Layout:
                HandleUtility.AddControl(controlID, HandleUtility.DistanceToCircle(drawPosition, HANDLE_SIZE));
                break ;

              case EventType.Repaint:
                DrawDrawMarker(drawPosition, normal) ;
                Handles.DrawDottedLine(OffsetPosition(drawPosition, normal), point, HANDLE_LINE_SIZE) ;
                break;

              case EventType.MouseDown:
                if (HandleUtility.nearestControl != controlID || Event.current.button != 0) break ;
                GUIUtility.hotControl = controlID;
                Event.current.Use();
                break;

              case EventType.MouseUp:
                if (GUIUtility.hotControl != controlID || Event.current.button != 0) break ;

                Undo.RecordObject(builder.target, "Added new curve");
                var snappedPosition = drawSnap == 0 ?
                  OffsetPosition(drawPosition, normal) :
                  SnapVector(drawPosition, drawSnap);
                builder.target.Add(new CubicBezier(RemoveTranslation(point), RemoveTranslation(snappedPosition))) ;
                GUIUtility.hotControl = 0;
                Event.current.Use();
                break;

              case EventType.MouseMove:
                if(GUIUtility.hotControl == 0)
                SceneView.RepaintAll();
                break;
        }
      }

      private Vector3 SnapVector (Vector3 vector, float factor = 1f) {
        factor = Mathf.Clamp(factor, Mathf.Epsilon, 1f) ;
        vector[0] = Mathf.Round(vector[0] / factor) * factor ;
        vector[1] = Mathf.Round(vector[1] / factor) * factor ;
        vector[2] = Mathf.Round(vector[2] / factor) * factor ;
        return vector ;
      }

      private Vector3 GetLimitedPlaneNormal (AxisLimiter limit) {
        if(limit == AxisLimiter.Z)
          return Vector3.forward;
        if(limit == AxisLimiter.X)
          return Vector3.right ;
        return Vector3.up ;
      }

      private Vector3 OffsetPosition (Vector3 center, Vector3 normal) {
        return center - (-normal * planeOffset) ;
      }

      private void DrawDrawMarker(Vector3 center, Vector3 normal) {
        var quaternion = Quaternion.FromToRotation(Vector3.forward, normal) ;
        var col = Handles.color ;
        var positionOnPlane = OffsetPosition(center, normal) ;
        Handles.RectangleHandleCap(-1, positionOnPlane, quaternion, HANDLE_SIZE, EventType.Repaint);
        Handles.CircleHandleCap(-1, positionOnPlane, quaternion, HANDLE_SIZE * .1f, EventType.Repaint);

        Handles.color = Color.gray;
        Handles.DrawDottedLine(center, positionOnPlane, HANDLE_LINE_SIZE) ;
        Handles.RectangleHandleCap(-1, center, quaternion, HANDLE_SIZE, EventType.Repaint);
        Handles.CircleHandleCap(-1, center, quaternion, HANDLE_SIZE * .1f, EventType.Repaint);
        Handles.color = col;
      }


      // //////////////// //
      //  Move Tool
      // //////////////// //
      private int selectedMoveHandle = -1;
      private enum MoveToolType { Free, Normal } ;
      private MoveToolType curMoveToolType = MoveToolType.Normal ;

      private void MoveToolUI () {
        var oldType = curMoveToolType ;
        curMoveToolType = (MoveToolType)EditorGUILayout.EnumPopup(new GUIContent("Move method: "), curMoveToolType);
        if(oldType != curMoveToolType) {
          selectedMoveHandle = -1 ;
          SceneView.RepaintAll() ;
        }
      }

      private void DoMoveTool () {
        var points = builder.target.GetControlPoints() ;
        if(points.Length == 0) return ;
        System.Action<int, Vector3> method = DrawMoveHandle;
        if(curMoveToolType == MoveToolType.Free) method = DrawFreeMoveHandle ;

        Handles.color = Color.black ;
        Vector3 prev = TranslatePoint(points[0]) ;
        method(0, prev) ;
        for(int cnt = 1; cnt < points.Length; cnt ++) {
          var cur = TranslatePoint(points[cnt]) ;
          Handles.color = Color.white ;
          Handles.DrawDottedLine(prev, cur, HANDLE_LINE_SIZE) ;
          Handles.color = Color.black ;
          method(cnt, cur) ;
          prev = cur ;
        }
      }

      private void DrawFreeMoveHandle (int i, Vector3 point) {
        EditorGUI.BeginChangeCheck();
        Vector3 newPoint = Handles.FreeMoveHandle(point, Quaternion.identity, HANDLE_SIZE, Vector3.zero, Handles.RectangleHandleCap) ;

        if(EditorGUI.EndChangeCheck()) {
          Undo.RecordObject(builder.target, "Changed ControlPoint position");
          builder.target.SetControlPoint(i, RemoveTranslation(newPoint)) ;
        }
      }

      private void DrawMoveHandle (int i, Vector3 point) {
        if(selectedMoveHandle != i) {
          if(Handles.Button(point, Quaternion.identity, HANDLE_SIZE, HANDLE_SIZE, Handles.CubeHandleCap))
            selectedMoveHandle = i ;
          return ;
        }

        DrawVectorGUI(i, point) ;

        EditorGUI.BeginChangeCheck();
        Vector3 newPoint = Handles.PositionHandle(point, Quaternion.identity) ;

        if(EditorGUI.EndChangeCheck()) {
          Undo.RecordObject(builder.target, "Changed ControlPoint position");
          builder.target.SetControlPoint(i, RemoveTranslation(newPoint)) ;
        }
      }

      private void DrawVectorGUI (int i, Vector3 point) {
        Handles.BeginGUI();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.MaxWidth(200)) ;
              EditorGUILayout.Space();
              EditorGUILayout.BeginHorizontal(EditorStyles.miniButtonLeft) ;
                EditorGUILayout.LabelField("Control point") ;
              EditorGUILayout.EndHorizontal() ;

              EditorGUILayout.Space() ;
              EditorGUI.BeginChangeCheck();
              Vector3 newPoint = EditorGUILayout.Vector3Field(new GUIContent(""), point);

              if(EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(builder.target, "Changed ControlPoint position");
                builder.target.SetControlPoint(i, RemoveTranslation(newPoint)) ;
              }

              EditorGUILayout.Space();
            EditorGUILayout.EndVertical() ;
        Handles.EndGUI();
      }

  }
}
