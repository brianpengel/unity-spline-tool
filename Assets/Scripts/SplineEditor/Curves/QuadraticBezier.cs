// using System.Collections.Generic ;
// using System.Collections ;
// using UnityEngine ;
//
//
// namespace CustomSplines {
//   [System.Serializable]
//   public class QuadraticBezier : Curve {
//
//     public QuadraticBezier () : base(1) {}
//
//     public override Vector3 GetInterpolatedPoint(float t) {
//       float a = 1 - t ;
//       return a * a * points[i] + 2f * a * t * points[i + 1] + t * t * points[i + 2];
//     }
//
//     public override Vector3 GetDerivative (float t) {
//       float a = 1 - t ;
//   		return 2f * a * (points[i + 1] - points[i]) + 2f * t * (points[i + 2] - points[i + 1]);
//   	}
//   }
// }
