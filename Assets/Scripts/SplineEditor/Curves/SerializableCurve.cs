using System ;
using UnityEngine ;
using TypeReferences ;

namespace CustomSplines {

  [System.Serializable]
  internal struct SerializableCurve {
    [HideInInspector] [SerializeField] public ClassTypeReference type ;
    [HideInInspector] [SerializeField] public Vector3[] handles ;

    private SerializableCurve(Curve c) {
      type = c.GetType() ;
      handles = c.GetHandles() ;
    }

    public Curve Deserialize() {
      var c = (Curve)Activator.CreateInstance((Type)type, handles[0], handles[handles.Length - 1]) ;
      for(int cnt = 1; cnt < handles.Length - 1; cnt ++)
        c.SetHandle(cnt, handles[cnt]) ;
      return c ;
    }

    public static SerializableCurve Serialize(Curve curve) {
      return new SerializableCurve(curve) ;
    }
  }

}
