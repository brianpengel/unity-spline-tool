using System.Collections.Generic ;
using System.Collections ;
using UnityEngine ;


namespace CustomSplines {
  [System.Serializable]
  public class CubicBezier : Curve {

    public CubicBezier (Vector3 start, Vector3 end) : base(start, end, 4) {}

    public override Vector3 GetInterpolatedPoint(float t) {
      float a = 1 - t ;
      Vector3 p0 = handles[0],  p1 = handles[1],
              p2 = handles[2],  p3 = handles[3];

      return a * a * a * p0 + 3f * a * a * t * p1 + 3f * a * t * t * p2 + t * t * t * p3;
    }


    public override Vector3 GetDerivative (float t) {
      float a = 1 - t ;
      Vector3 p0 = handles[0],  p1 = handles[1],
              p2 = handles[2],  p3 = handles[3];

  		return 3f * a * a * (p1 - p0) + 6f * a * t * (p2 - p1) + 3f * t * t * (p3 - p2);
  	}
  }
}
