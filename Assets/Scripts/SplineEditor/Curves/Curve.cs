using System.Collections.Generic ;
using System.Collections ;

using System.Reflection;
using System;

using UnityEngine ;

namespace CustomSplines {

  [System.Serializable]
  public abstract class Curve {

    public readonly static int RESOLUTION_STEPS = 10 ;

    [SerializeField] protected Vector3[] handles ;

    public Vector3 FirstHandle  { get { return handles[0]; } }
    public Vector3 LastHandle   { get { return handles[Count-1]; } }
    public float Length         { get; private set; }
    public int Count            { get { return handles.Length ; } }


    protected Curve (Vector3 start, Vector3 end, int handleCount) {
      handles = new Vector3[handleCount] ;
      handles[0] = start ;
      handles[Count - 1] = end ;
      ResetHandles() ;
      RecalculteLength() ;
    }

    public void ResetHandles() {
      float step = 1f / (Count - 1) ;
      for(int cnt = 1; cnt < Count - 1; cnt ++)
        handles[cnt] = Vector3.Lerp(handles[0], handles[Count - 1], step * (cnt)) ;
    }

    public Vector3 GetNormal (float t, Vector3 up) {
      Vector3 tangent = GetTangent(t),
              binormal = Vector3.Cross(up, tangent).normalized ;
  		return Vector3.Cross(tangent, binormal) ;
  	}

    public Vector3 GetTangent (float t) {
  		return GetDerivative(t).normalized;
  	}

    private void RecalculteLength () {
      Vector3 prev = handles[0] ;
      float stepT = 1f / (RESOLUTION_STEPS - 1),
            newLength = 0f ;

      for (int cnt = 1; cnt < RESOLUTION_STEPS; cnt++) {
        Vector3 current = GetInterpolatedPoint(stepT * cnt) ;
        float dist = Vector3.Distance(current, prev) ;
        newLength += dist ;
        prev = current ;
      }

      Length = newLength ;
    }

    public void SetHandle(int i, Vector3 val) {
      handles[i] = val ;
      RecalculteLength () ;
    }

    public Vector3 GetHandle(int i) {
      return handles[i] ;
    }

    public Vector3[] GetHandles () {
      return (Vector3[])handles.Clone();
    }

    // /////////////////// //
    // Abstracts
    // /////////////////// //
    public abstract Vector3 GetInterpolatedPoint(float t) ;
    public abstract Vector3 GetDerivative       (float t) ;

    // /////////////////// //
    // Statics
    // /////////////////// //
    public static Dictionary<string, Type> GetCurveTypes() {
      Type targetType = typeof(Curve) ;
      Type[] types =  Assembly.GetAssembly(targetType).GetTypes() ;
      var foundTypes = new Dictionary<string, Type> () ;

      foreach(Type type in types)
        if (type.IsClass && !type.IsAbstract && type.IsSubclassOf(targetType))
          foundTypes.Add(type.Name, type) ;

      return foundTypes ;
    }
  }
}
